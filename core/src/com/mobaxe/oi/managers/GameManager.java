package com.mobaxe.oi.managers;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mobaxe.oi.ActionResolver;
import com.mobaxe.oi.GameState;
import com.mobaxe.oi.helpers.AnimatedSprite;
import com.mobaxe.oi.helpers.Assets;
import com.mobaxe.oi.helpers.Box2DFactory;
import com.mobaxe.oi.helpers.BoxProperties;
import com.mobaxe.oi.levels.Level;
import com.mobaxe.oi.levels.LevelFactory;
import com.mobaxe.oi.objects.I;
import com.mobaxe.oi.objects.O;
import com.mobaxe.oi.screens.MyScreens;
import com.mobaxe.oi.utils.Utils;

public class GameManager {

	public static OrthographicCamera camera;
	public static InputMultiplexer multiplexer;
	public static SpriteBatch batch;
	public static Stage stage;
	public static World world;

	private static FileHandle file;
	private static Vector2 pos;
	private static Vector2 spriteSize;
	private static String name;
	private static float loaderScale;
	private static BodyType bodyType;
	public static I i;
	public static O o;
	public static BoxProperties groundBox;
	public static volatile Body circleBody1;
	public static volatile Body circleBody2;

	public static BoxProperties line1;
	public static BoxProperties line2;
	public static BoxProperties line3;
	public static BoxProperties line4;

	public static float runTime;
	public static int lap;

	private static Table timerTable;
	private static Label timerLabel;
	private static Table pointTable;
	public static Label pointLabel;
	private static Table startTextTable;
	private static Label lblStartText;
	private static LabelStyle labelStyle;

	public static GameState gameState;
	private static InputManager inputManager;

	public static boolean firstRule;
	public static boolean secondRule;
	public static boolean thirdRule;
	public static boolean fourthRule;

	public static boolean isGameOver;
	public static volatile int currentLevel = 1;
	public static Level level;
	public static ActionResolver actionResolver;
	public static int gameOverCount;

	public static void init(World w, OrthographicCamera c, Stage s, SpriteBatch sb, InputMultiplexer im,
			ActionResolver ar) {
		world = w;
		camera = c;
		stage = s;
		batch = sb;
		multiplexer = im;
		gameState = GameState.READY_TO_START;
		isGameOver = false;
		actionResolver = ar;
		inputManager = new InputManager();
		multiplexer.addProcessor(inputManager);
	}

	public static void initWorld() {

		createGroundBox();

		Texture elips = Assets.loadTexture("images/elips.png");
		currentLevel = 4;
		level = LevelFactory.getLevelInstance(currentLevel);
		createCircleForO(elips, level.oCirclePosition);
		createCircleForI(elips, level.iCirclePosition);

		createI(level.Iposition);
		createO(level.Oposition);

		createPointLines(level.oSquare1, level.oSquare2, level.ISquare1, level.ISquare2);

		labelStyle = new LabelStyle();
		labelStyle.font = Assets.font;
		timerTable = new Table();
		timerTable.setFillParent(true);
		timerLabel = new Label("0,0''", labelStyle);
		timerTable.add(timerLabel).pad(0, 0, 415, 660);

		pointTable = new Table();
		pointTable.setFillParent(true);
		pointLabel = new Label("LAP:", labelStyle);
		pointTable.add(pointLabel).pad(0, 500, 415, 0);

		startTextTable = new Table();
		startTextTable.setFillParent(true);
		lblStartText = new Label("", labelStyle);
		startTextTable.add(lblStartText).pad(0, 0, 415, 0);

		stage.addActor(timerTable);
		stage.addActor(pointTable);
		stage.addActor(startTextTable);

	}

	private static void createPointLines(Vector2 l1, Vector2 l2, Vector2 l3, Vector2 l4) {
		line1 = new BoxProperties(world, 3f, 1f, l1, BodyType.StaticBody, true, Utils.TEST_FILTER);
		line2 = new BoxProperties(world, 3f, 1f, l2, BodyType.StaticBody, true, Utils.TEST_FILTER);
		line3 = new BoxProperties(world, 3f, 1f, l3, BodyType.StaticBody, true, Utils.TEST_FILTER);
		line4 = new BoxProperties(world, 3f, 1f, l4, BodyType.StaticBody, true, Utils.TEST_FILTER);
		if (currentLevel == 3 || currentLevel == 4) {
			line3.getBody().setTransform(line3.getBody().getPosition(), 1.57f);
			line4.getBody().setTransform(line4.getBody().getPosition(), 1.57f);
		}
	}

	private static void createO(Vector2 position) {
		Sprite sprite;
		file = Assets.loadJson("models/o.json");
		pos = new Vector2(position.x, position.y);
		spriteSize = new Vector2(15, 15);
		sprite = new Sprite(Assets.loadTexture("models/o.png"));
		loaderScale = 15;
		name = "o";
		bodyType = BodyType.StaticBody;

		o = new O(file, world, camera, sprite, bodyType, pos, spriteSize, loaderScale, name);
	}

	private static void createI(Vector2 position) {
		Sprite sprite;
		file = Assets.loadJson("models/i.json");
		pos = new Vector2(position.x, position.y);
		spriteSize = new Vector2(6, 15f);
		sprite = new Sprite(Assets.loadTexture("models/i.png"));
		loaderScale = 6.2f;
		name = "i";
		bodyType = BodyType.StaticBody;

		i = new I(file, world, camera, sprite, bodyType, pos, spriteSize, loaderScale, name);

		if (currentLevel == 3 || currentLevel == 4) {
			i.body.setTransform(i.body.getPosition(), 4.71f);
			sprite.rotate(-90);
		}

	}

	private static void createCircleForO(Texture elips, Vector2 position) {
		// CIRCLE 1
		FixtureDef fd = new FixtureDef();
		CircleShape shape = (CircleShape) Box2DFactory.createCircleShape(1.2f);
		fd.shape = shape;
		circleBody1 = Box2DFactory.createBody(world, BodyType.DynamicBody, fd, position, false);
		Sprite sprite = new Sprite(elips);
		sprite.setSize(2.3f, 2.3f);
		circleBody1.setUserData(sprite);
	}

	private static void createCircleForI(Texture elips, Vector2 position) {
		// CIRCLE 2
		FixtureDef fd2 = new FixtureDef();
		CircleShape shape2 = (CircleShape) Box2DFactory.createCircleShape(1.2f);
		fd2.shape = shape2;
		circleBody2 = Box2DFactory.createBody(world, BodyType.DynamicBody, fd2, position, false);
		Sprite sprite2 = new Sprite(elips);
		sprite2.setSize(2.3f, 2.3f);
		circleBody2.setUserData(sprite2);
	}

	private static void createGroundBox() {
		groundBox = new BoxProperties(world, camera.viewportWidth, 1f, new Vector2(0, -11),
				BodyType.StaticBody, false, (short) 0);
	}

	public static void showIntersititial() {

		if (gameOverCount == 3) {
			actionResolver.showOrLoadInterstital();
			gameOverCount = 0;
		}

	}

	public static void updateStages(float delta) {
		stage.act(delta);
		stage.draw();
	}

	public static void updateSprites(float delta) {
		batch.setProjectionMatrix(camera.combined);

		i.draw(batch);
		o.draw(batch);

		Sprite circleSprite = (Sprite) circleBody1.getUserData();
		batch.begin();
		circleSprite.setPosition(circleBody1.getPosition().x - circleSprite.getWidth() / 2,
				circleBody1.getPosition().y - circleSprite.getHeight() / 2);
		circleSprite.draw(batch);
		batch.end();

		Sprite circleSprite2 = (Sprite) circleBody2.getUserData();
		batch.begin();
		circleSprite2.setPosition(circleBody2.getPosition().x - circleSprite2.getWidth() / 2,
				circleBody2.getPosition().y - circleSprite2.getHeight() / 2);
		circleSprite2.draw(batch);
		batch.end();

		runTime += delta;
		drawTimer(runTime);

		if (lap >= level.lap && runTime <= level.time) {
			actionResolver.showOrLoadInterstital();
			PreferencesManager.saveScore(lap);
			PreferencesManager.saveTime(runTime);
			currentLevel++;
			runTime = 0;
			lap = 0;
			gameState = GameState.READY_TO_START;
			fourthRule = false;
			thirdRule = false;
			secondRule = false;
			firstRule = false;
			inputManager.tempBody = null;
			inputManager.hitBody = null;
			inputManager.mouseJoint = null;
			ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
			ScreenManager.getInstance().show(MyScreens.CONGRATS_SCREEN);
		} else if (runTime > level.time && lap < level.lap) {
			actionResolver.showOrLoadInterstital();
			PreferencesManager.saveScore(lap);
			PreferencesManager.saveTime(runTime);
			runTime = 0;
			lap = 0;
			gameState = GameState.READY_TO_START;
			fourthRule = false;
			thirdRule = false;
			secondRule = false;
			firstRule = false;
			inputManager.tempBody = null;
			inputManager.hitBody = null;
			inputManager.mouseJoint = null;
			ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
			ScreenManager.getInstance().show(MyScreens.GAME_OVER_SCREEN);
		}

		if (isGameOver == true) {
			gameOverCount++;
			showIntersititial();
			PreferencesManager.saveScore(lap);
			PreferencesManager.saveTime(runTime);
			runTime = 0;
			lap = 0;
			gameState = GameState.READY_TO_START;
			fourthRule = false;
			thirdRule = false;
			secondRule = false;
			firstRule = false;
			isGameOver = false;

			inputManager.tempBody = null;
			inputManager.hitBody = null;
			inputManager.mouseJoint = null;
			ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
			ScreenManager.getInstance().show(MyScreens.GAME_OVER_SCREEN);
		}

	}

	public static void drawTimer(float timer) {
		if (gameState == GameState.READY_TO_START) {
			lblStartText.setText("TAP TO SCREEN");
			runTime = 0;
		} else {
			// if (startTextTable != null) {
			// startTextTable.remove();
			// }
			lblStartText.setText("GOAL: " + level.time + "''" + " AND " + level.lap + " LAP");
			String time = String.format("%.1f", timer);
			timerLabel.setText(time + "''");
		}

	}

	public static void createContactListener() {
		world.setContactListener(new ContactListener() {

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}

			@Override
			public void endContact(Contact contact) {
			}

			@Override
			public void beginContact(Contact contact) {
				Body bodyA = contact.getFixtureA().getBody();
				Body bodyB = contact.getFixtureB().getBody();

				if (bodyB.equals(circleBody1) && bodyA.equals(line1.getBody())) {
					firstRule = true;
				}
				if (bodyB.equals(circleBody1) && bodyA.equals(line2.getBody())) {
					secondRule = true;
				}
				if (bodyB.equals(circleBody2) && bodyA.equals(line3.getBody())) {
					thirdRule = true;
				}
				if (bodyB.equals(circleBody2) && bodyA.equals(line4.getBody())) {
					fourthRule = true;
				}
				if (gameState == GameState.STARTED) {
					if (bodyB.equals(circleBody1) && bodyA.equals(o.body) || bodyB.equals(circleBody2)
							&& bodyA.equals(i.body)) {

						if (isGameOver == false) {
							isGameOver = true;
						}

					}
				}
			}
		});
	}
}
