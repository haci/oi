package com.mobaxe.oi.managers;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.mobaxe.oi.GameState;

public class InputManager extends InputAdapter {

	/* multiple mouse joint experiment */
	public MouseJoint mouseJoint[];
	public Body hitBody[];
	public Body tempBody;

	public InputManager() {
		mouseJoint = new MouseJoint[2];
		hitBody = new Body[2];
	}

	/**
	 * a temporary vector for delta target destination during touchDragged()
	 * method
	 **/
	Vector2 target = new Vector2();
	/*------------------------helper methods------------------------------------*/
	/* android screen touch vector for a mouse joint */
	Vector3 testPoint = new Vector3(); // we instantiate this vector and the
										// callback here so we don't irritate
										// the GC
	QueryCallback callback = new QueryCallback() {
		@Override
		public boolean reportFixture(Fixture fixture) {
			// if the hit fixture's body is the ground body
			// we ignore it
			if (fixture.getBody() == GameManager.groundBox.getBody()
					|| fixture.getBody() == GameManager.i.body || fixture.getBody() == GameManager.o.body)
				return true;

			if (fixture.testPoint(testPoint.x, testPoint.y)) {
				tempBody = fixture.getBody();
				return false;
			} else
				return true;
		}
	};

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (GameManager.gameState == GameState.READY_TO_START) {
			GameManager.gameState = GameState.STARTED;
		}
		testPoint.set(screenX, screenY, 0);
		GameManager.camera.unproject(testPoint);

		// ask the world which bodies are within the given
		// bounding box around the mouse pointer

		if (pointer <= 1) {
			if (!GameManager.isGameOver) {
				if (hitBody[pointer] != null) {
					hitBody[pointer] = null;
				}
				GameManager.world.QueryAABB(callback, testPoint.x - 1.0f, testPoint.y - 1.0f,
						testPoint.x + 1.0f, testPoint.y + 1.0f);

				if (tempBody != null) {
					if (tempBody == GameManager.circleBody1 || tempBody == GameManager.circleBody2) {
						hitBody[pointer] = tempBody;

						// if we hit something we create a new mouse joint
						// and attach it to the hit body.
						if (hitBody[pointer] != null) {

							MouseJointDef def = new MouseJointDef();
							def.bodyA = GameManager.groundBox.getBody();
							def.bodyB = hitBody[pointer];
							def.frequencyHz = 1000;
							def.collideConnected = true;
							def.target
									.set(hitBody[pointer].getPosition().x, hitBody[pointer].getPosition().y);
							def.maxForce = 30000.0f * hitBody[pointer].getMass();
							def.dampingRatio = 0;

							mouseJoint[pointer] = (MouseJoint) GameManager.world.createJoint(def);
							hitBody[pointer].setAwake(true);
						}
						tempBody = null;
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if (pointer <= 1) {
			if (!GameManager.isGameOver) {
				if (hitBody[pointer] != null) {
					if (hitBody[pointer] == GameManager.circleBody1
							|| hitBody[pointer] == GameManager.circleBody2) {
						if (mouseJoint[pointer] != null) {
							GameManager.camera.unproject(testPoint.set(screenX, screenY, 0));
							mouseJoint[pointer].setTarget(target.set(testPoint.x, testPoint.y));
						}
						if (GameManager.firstRule == true && GameManager.secondRule == true
								&& GameManager.thirdRule == true && GameManager.fourthRule == true) {
							GameManager.firstRule = false;
							GameManager.secondRule = false;
							GameManager.thirdRule = false;
							GameManager.fourthRule = false;
							GameManager.lap++;
							GameManager.pointLabel.setText("LAP:" + GameManager.lap);
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (pointer <= 1) {
			if (!GameManager.isGameOver) {
				if (mouseJoint[pointer] != null) {
					GameManager.world.destroyJoint(mouseJoint[pointer]);
					mouseJoint[pointer] = null;
				}
				GameManager.firstRule = false;
				GameManager.secondRule = false;
				GameManager.thirdRule = false;
				GameManager.fourthRule = false;

				if (hitBody[pointer] == GameManager.circleBody1
						|| hitBody[pointer] == GameManager.circleBody2) {
					hitBody[pointer].setLinearVelocity(new Vector2(0, 0));
				}
			}
		}
		return false;
	}

}
