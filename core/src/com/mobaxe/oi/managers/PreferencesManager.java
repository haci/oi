package com.mobaxe.oi.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class PreferencesManager {

	private static String SCORE = "score";
	private static String TIME = "time";

	private static Preferences prefs = Gdx.app.getPreferences("io");

	public static void saveScore(int score) {
		prefs.putInteger(SCORE, score);
		prefs.flush();

	}

	public static int getScore() {
		return prefs.getInteger(SCORE);
	}

	public static void saveTime(float time) {
		prefs.putFloat(TIME, time);
		prefs.flush();
	}

	public static float getTime() {
		return prefs.getFloat(TIME);
	}

	public static void clearPrefs() {
		prefs.clear();
	}

}