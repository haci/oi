package com.mobaxe.oi;

public enum GameState {
	READY_TO_START, STARTED
}
