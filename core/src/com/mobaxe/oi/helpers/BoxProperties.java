package com.mobaxe.oi.helpers;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class BoxProperties {

	private Body body;
	private float width;
	private float height;

	public float getHeight() {
		return height;
	}

	public float getWidth() {
		return width;
	}

	public Body getBody() {
		return body;
	}

	public BoxProperties(World world, float width, float height, Vector2 position, BodyType bodyType,
			boolean isSensor, short filter) {

		// initialize body
		BodyDef bodyDef = new BodyDef();
		bodyDef.position.set(position);
		bodyDef.angle = 0;
		bodyDef.fixedRotation = true;
		body = world.createBody(bodyDef);
		body.setType(bodyType);

		// initialize shape
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.filter.groupIndex = filter;
		fixtureDef.isSensor = isSensor;
		PolygonShape boxShape = new PolygonShape();
		boxShape.setAsBox(width / 2, height / 2);
		this.width = width;
		this.height = height;
		fixtureDef.shape = boxShape;
		fixtureDef.restitution = 0f; // positively bouncy!
		this.body.createFixture(fixtureDef);
		boxShape.dispose();
	}
}
