package com.mobaxe.oi.helpers;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class Assets {

	private static FreeTypeFontGenerator generator;
	private static FreeTypeFontParameter parameter;
	public static BitmapFont font;

	private static Sound sound;
	private static Music music;

	public static Texture splash, mainMenu, moreApp, moreAppActive, play, playActive, rate, rateActive,
			gameOver, congratz, theEnd;

	public static Texture loadTexture(String filePath) {
		return new Texture(Gdx.files.internal(filePath));
	}

	public static Sound loadSound(String filePath) {
		sound = Gdx.audio.newSound(Gdx.files.internal(filePath));
		return sound;
	}

	public static Music loadMusic(String filePath) {
		music = Gdx.audio.newMusic(Gdx.files.internal(filePath));
		return music;
	}

	public static FileHandle loadJson(String filePath) {
		return Gdx.files.internal(filePath);
	}

	public static TextureAtlas loadAtlas(String filePath) {
		return new TextureAtlas(Gdx.files.internal(filePath));
	}

	public static void loadTexturesOnCreate() {
		splash = loadTexture("images/splash.png");
		mainMenu = loadTexture("images/main.jpg");
		moreApp = loadTexture("images/moreapp.png");
		moreAppActive = loadTexture("images/moreappactive.png");
		play = loadTexture("images/play.png");
		playActive = loadTexture("images/playactive.png");
		rate = loadTexture("images/rateup.png");
		rateActive = loadTexture("images/rateupactive.png");
		gameOver = loadTexture("images/gameover.jpg");
		congratz = loadTexture("images/congrats.jpg");
		theEnd = loadTexture("images/theend.jpg");
		// FONT
		generateFont();

	}

	private static void generateFont() {
		generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/text.ttf"));
		parameter = new FreeTypeFontParameter();
		int density = 45;
		if (ApplicationType.Android == Gdx.app.getType()) {
			density = 12;
		}

		parameter.size = (int) (density * Gdx.graphics.getDensity());
		font = generator.generateFont(parameter);

		generator.dispose(); // don't forget to dispose to avoid memory leaks!

	}

	public static void dispose() {
	}
}