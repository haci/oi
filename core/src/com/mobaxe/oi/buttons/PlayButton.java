package com.mobaxe.oi.buttons;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.oi.helpers.Assets;
import com.mobaxe.oi.managers.GameManager;
import com.mobaxe.oi.managers.ScreenManager;
import com.mobaxe.oi.screens.MyScreens;

public class PlayButton extends Button {

	private String buttonUp;
	private Skin skin;
	private ButtonStyle style;
	private String buttonDown;
	public static int clickCounter;

	public PlayButton() {
		buttonUp = "ButtonUp";
		buttonDown = "ButtonDown";
		initSkins();
		clickListener();
	}

	private void initSkins() {
		skin = new Skin();

		skin.add(buttonUp, Assets.play);
		skin.add(buttonDown, Assets.playActive);
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.down = skin.getDrawable(buttonDown);

		setStyle(style);
	}

	private void clickListener() {

		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				clickCounter++;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					if (clickCounter == 1) {
						startGame();
						clickCounter = 0;
					}
				} else {
					clickCounter--;
				}
			}

		});
	}

	public void startGame() {
		ScreenManager.getInstance().dispose(MyScreens.MAIN_MENU_SCREEN);
		ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
	}
}