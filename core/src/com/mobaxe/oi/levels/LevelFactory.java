package com.mobaxe.oi.levels;

public class LevelFactory {

	public static Level getLevelInstance(int level) {
		return new Level(level);
	}

}
