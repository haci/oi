package com.mobaxe.oi.levels;

import com.badlogic.gdx.math.Vector2;

public class Level {

	private int level;
	public Vector2 oCirclePosition;
	public Vector2 iCirclePosition;
	public Vector2 Oposition;
	public Vector2 Iposition;

	// SCORE SQUARES
	public Vector2 oSquare1;
	public Vector2 oSquare2;
	public Vector2 ISquare1;
	public Vector2 ISquare2;
	public int lap;
	public int time;

	public Level(int level) {
		this.level = level;
		initLevelDatas();
	}

	private void initLevelDatas() {
		oCirclePosition = new Vector2();
		iCirclePosition = new Vector2();
		Oposition = new Vector2();
		Iposition = new Vector2();

		oSquare1 = new Vector2();
		oSquare2 = new Vector2();
		ISquare1 = new Vector2();
		ISquare2 = new Vector2();

		switch (level) {
		case 1:
			oCirclePosition.set(-7.7f, 4.7f);
			iCirclePosition.set(13.4f, 0);
			Iposition.set(10.7f, -8);
			Oposition.set(-15, -8);

			oSquare1.set(-12.7f, 0);
			oSquare2.set(-2.4f, 0);
			ISquare1.set(13.8f, 4.5f);
			ISquare2.set(13.8f, -5);

			time = 30;
			lap = 30;

			break;
		case 2:
			oCirclePosition.set(8f, 4.7f);
			iCirclePosition.set(-11f, 0);
			Iposition.set(-14f, -8);
			Oposition.set(1, -8);

			oSquare1.set(3, 0);
			oSquare2.set(13.5f, 0);
			ISquare1.set(-11f, 4.5f);
			ISquare2.set(-11f, -5);

			time = 30;
			lap = 25;

			break;
		case 3:
			oCirclePosition.set(8f, 4.7f);
			iCirclePosition.set(-9f, -1);
			Iposition.set(-17f, 2);
			Oposition.set(1, -8);

			oSquare1.set(3, 0);
			oSquare2.set(13.5f, 0);
			ISquare1.set(-14f, -1);
			ISquare2.set(-5f, -1);

			time = 20;
			lap = 25;

			break;
		case 4:
			oCirclePosition.set(-7.7f, 4.7f);
			iCirclePosition.set(9, -1);
			Iposition.set(1.5f, 2);
			Oposition.set(-15, -8);

			oSquare1.set(-12.7f, 0);
			oSquare2.set(-2.4f, 0);
			ISquare1.set(5, -1);
			ISquare2.set(14, -1);

			time = 20;
			lap = 25;

			break;
		}
	}
}