package com.mobaxe.oi;

import com.badlogic.gdx.Game;
import com.mobaxe.oi.helpers.Assets;
import com.mobaxe.oi.managers.ScreenManager;
import com.mobaxe.oi.screens.MyScreens;

public class GameClassOI extends Game {

	public static ActionResolver actionResolver;

	public GameClassOI(ActionResolver actionResolver) {
		GameClassOI.actionResolver = actionResolver;
	}

	@Override
	public void create() {
		Assets.loadTexturesOnCreate();
		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().show(MyScreens.SPLASH_SCREEN);
	}

	@Override
	public void dispose() {
		ScreenManager.getInstance().dispose();
		Assets.dispose();
	}
}
