package com.mobaxe.oi.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.oi.ActionResolver;
import com.mobaxe.oi.managers.GameManager;
import com.mobaxe.oi.utils.Utils;

public class GameScreen implements Screen {

	private OrthographicCamera camera;
	private World world;
	private Box2DDebugRenderer debugRenderer;
	private SpriteBatch batch;
	private Stage stage;
	private InputMultiplexer multiplexer;
	private ActionResolver actionResolver;

	public GameScreen(ActionResolver actionResolver) {
		this.actionResolver = actionResolver;
	}

	@Override
	public void show() {

		veryFirstInit();

		GameManager.init(world, camera, stage, batch, multiplexer, actionResolver);

		GameManager.initWorld();
		GameManager.createContactListener();

		Gdx.input.setInputProcessor(multiplexer);

	}

	private void veryFirstInit() {
		multiplexer = new InputMultiplexer();
		world = new World(Utils.GRAVITY, true);
		camera = new OrthographicCamera(Utils.widthMeters, Utils.heightMeters);
		debugRenderer = new Box2DDebugRenderer();
		batch = new SpriteBatch();
		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(255 / 255f, 204 / 255f, 102 / 255f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		GameManager.updateStages(delta);
		GameManager.updateSprites(delta);
		world.step(delta, Utils.VELOCITY_ITERATIONS, Utils.POSITION_ITERATIONS);
//		debugRenderer.render(world, camera.combined);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {

	}
}
