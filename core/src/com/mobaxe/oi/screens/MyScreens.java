package com.mobaxe.oi.screens;

import com.badlogic.gdx.Screen;
import com.mobaxe.oi.GameClassOI;

public enum MyScreens {

	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen(GameClassOI.actionResolver);
		}
	},
	CONGRATS_SCREEN {
		public Screen getScreenInstance() {
			return new CongratzScreen();
		}
	},
	MAIN_MENU_SCREEN {
		public Screen getScreenInstance() {
			return new MainMenuScreen();
		}
	},
	GAME_OVER_SCREEN {
		public Screen getScreenInstance() {
			return new GameOverScreen();
		}
	},
	THE_END_SCREEN {
		public Screen getScreenInstance() {
			return new TheEndScreen();
		}
	},
	SPLASH_SCREEN {
		public Screen getScreenInstance() {
			return new SplashScreen();
		}
	};
	public abstract Screen getScreenInstance();

}
