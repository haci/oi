package com.mobaxe.oi.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.oi.buttons.MoreAppsButton;
import com.mobaxe.oi.buttons.PlayButton;
import com.mobaxe.oi.buttons.RateButton;
import com.mobaxe.oi.buttons.RefreshGameButton;
import com.mobaxe.oi.helpers.Assets;
import com.mobaxe.oi.managers.PreferencesManager;
import com.mobaxe.oi.utils.Utils;

public class GameOverScreen implements Screen {

	private Stage stage;
	private Table bgTable;
	private Texture bgTexture;
	private Sprite bgSprite;
	private Image bgImage;
	private Table scoreTable;
	private Table timeTable;
	private Label scoreLabel;
	private Label timeLabel;
	private LabelStyle labelStyle;

	private Table btnPlayTable;
	private Table rateAndMoreAppTable;

	public GameOverScreen() {

		stage = new Stage(new StretchViewport(Utils.virtualWidth, Utils.virtualHeight));
		bgTable = new Table();
		btnPlayTable = new Table();
		rateAndMoreAppTable = new Table();

		labelStyle = new LabelStyle();
		labelStyle.font = Assets.font;

		timeTable = new Table();
		timeTable.setFillParent(true);
		timeLabel = new Label("", labelStyle);
		String time = String.format("%.1f", PreferencesManager.getTime());
		timeLabel.setText(time);
		timeTable.add(timeLabel).pad(0, 0, 30, 480);
		// timeTable.pad(top, left, bottom, right)

		scoreTable = new Table();
		scoreTable.setFillParent(true);
		scoreLabel = new Label("", labelStyle);
		String scr = String.valueOf(PreferencesManager.getScore());
		scoreLabel.setText(scr);
		scoreTable.add(scoreLabel).pad(100, 0, 0, 480);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {

		bgTable.setFillParent(true);
		btnPlayTable.setFillParent(true);
		rateAndMoreAppTable.setFillParent(true);

		RefreshGameButton rgb = new RefreshGameButton();
		btnPlayTable.add(rgb).pad(0, 400, 155, 0);

		MoreAppsButton mab = new MoreAppsButton();
		RateButton rb = new RateButton();

		rateAndMoreAppTable.add(mab).pad(275, 400, 0, 0);
		rateAndMoreAppTable.add(rb).pad(275, 25, 15, 0);

		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);
		stage.addActor(bgTable);
		stage.addActor(btnPlayTable);
		stage.addActor(rateAndMoreAppTable);
		stage.addActor(timeTable);
		stage.addActor(scoreTable);
		stage.addAction(Actions.sequence(Actions.moveBy(-800, 0), Actions.moveTo(0, 0, 0.5f)));

	}

	private void setBackgroundImage() {
		bgTexture = Assets.gameOver;

		bgSprite = new Sprite(bgTexture);
		bgSprite.setColor(0, 0, 0, 0);
		bgSprite.setX(Utils.virtualWidth - bgSprite.getWidth() / 2);
		bgSprite.setY(Utils.virtualHeight - bgSprite.getHeight() / 2);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		stage = null;
		bgTable = null;
		bgTexture = null;
		bgSprite = null;
		bgImage = null;

		btnPlayTable = null;
		rateAndMoreAppTable = null;
	}

}