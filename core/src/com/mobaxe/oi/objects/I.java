package com.mobaxe.oi.objects;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.mobaxe.oi.helpers.BodyEditorLoader;
import com.mobaxe.oi.helpers.Box2DFactory;

public class I {

	public BodyEditorLoader bodyEditorLoader;
	public Body body;
	public Vector2 origin;
	public Vector2 position;
	private FixtureDef fixtureDef;
	private Sprite sprite;

	public I(FileHandle file, final World world, OrthographicCamera camera, Sprite sprite, BodyType bodyType,
			Vector2 pos, Vector2 spriteSize, float loaderScale, String name) {
		bodyEditorLoader = new BodyEditorLoader(file);
		fixtureDef = Box2DFactory.createFixture(new PolygonShape(), 1, 0, 0, false);
		body = Box2DFactory.createBody(world, bodyType, fixtureDef, pos, false);

		bodyEditorLoader.attachFixture(body, name, fixtureDef, loaderScale);
		origin = bodyEditorLoader.getOrigin(name, loaderScale);
		position = body.getPosition().sub(origin);
		this.sprite = sprite;
		this.sprite.setSize(spriteSize.x, spriteSize.y);
		this.sprite.setOrigin(origin.x, origin.y);
		this.sprite.setPosition(position.x, position.y);

	}

	public void draw(SpriteBatch batch) {
		batch.begin();
		sprite.draw(batch);
		batch.end();
	}
}
